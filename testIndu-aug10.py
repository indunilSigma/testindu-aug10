import boto3
ddb = boto3.client("dynamodb")

def handler(event, context):
    try:
        data = ddb.query(
            TableName="induAug10",
            IndexName="induAug10-indusort2-index",
            ExpressionAttributeValues={
                ':attriIndu': {
                    'S': "dress1"
                }
            },
            KeyConditionExpression="induAug10 = :attriIndu"
        )
        print(data)
    except BaseException as e:
        print(e)
        raise(e)
    
    return {"message": "Successfully executed"}
